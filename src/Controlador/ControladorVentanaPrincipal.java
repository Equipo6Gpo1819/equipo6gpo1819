package Controlador;

import Alumno.Alumno;
import Modelo.ModeloAltaAlumno;
import Vista.VistaAltaAlumno;
import Vista.VistaListaAlumno;
import Vista.VistaVentanaPrincipal;
import java.awt.event.*;
import javax.swing.*;

public class ControladorVentanaPrincipal{
    private VistaVentanaPrincipal vista;

    public ControladorVentanaPrincipal(VistaVentanaPrincipal vista){
        this.vista = vista;
        EventosVP();
    }

    public void InciarVista(){
        vista.setResizable(false);
        vista.setLocation(283, 80);
        vista.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        vista.setTitle("Sistema de Control de Calificaciones");
        vista.setVisible(true);
        vista.setSize(500, 500);
        vista.setLocationRelativeTo(null);
    }
    
    public void EventosVP(){
        vista.oAgregar.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent ae) {
                VistaAltaAlumno val = new VistaAltaAlumno();
                ModeloAltaAlumno mal = new ModeloAltaAlumno();
                ControladorAltaAlumno al = new ControladorAltaAlumno(mal, val);
                al.IniciarVistaAltaAlumno();
           }
        });
        
        vista.oVerLista.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent ae) {
                Alumno a;
                
                VistaListaAlumno vla = new VistaListaAlumno();
                ControladorListaAlumno al = new ControladorListaAlumno(vla);
                al.IniciarVistaListaAlumno();
                
                for (int i = 0; i < ControladorAltaAlumno.lista.size(); i++) {
                a = (Alumno) ControladorAltaAlumno.lista.get(i); //en a se almacena lo obtenido en la posicion i de lista
                                      //a vale "los datos del alumno" en ese momento
                al.mostrarDatosTabla(a);
        }
           }
        
        });
        
        vista.oAcercaDe.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent ae) {
                JOptionPane.showMessageDialog(null, "SISTEMA DE CONTROL DE CALIFICACIONES \n"+"Creador: equipo 6 \n \n"+
                        "integrantes:\n"+"Ruiz Rotonda Luis Alberto \n"+ "Coutiño Salas Eduardo \n"+"Aquino Buasonó Marco Alejandro");
            }
        });
        
        vista.oSalir.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent ae) {
                System.exit(0);
            }
        });
    }
    
}