package Controlador;

import Alumno.Alumno;
import Vista.VistaListaAlumno;
import java.awt.event.*;
import javax.swing.JFrame;

public class ControladorListaAlumno {

    private VistaListaAlumno vistaLalu;
    
    int con = 0;

    public ControladorListaAlumno(VistaListaAlumno vistaLalu) {
        this.vistaLalu = vistaLalu;
        EventosLA();
    }
    
    public void mostrarDatosTabla(Alumno a){
        vistaLalu.modelo.insertRow(con, new Object[]{});
        vistaLalu.modelo.setValueAt(a.getClave(), con, 0);
        vistaLalu.modelo.setValueAt(a.getApellidoP(), con, 1);
        vistaLalu.modelo.setValueAt(a.getApellidoM(), con, 2);
        vistaLalu.modelo.setValueAt(a.getNombre(), con, 3);
        vistaLalu.modelo.setValueAt(a.getEspanol(), con, 4);
        vistaLalu.modelo.setValueAt(a.getMatematicas(), con, 5);
        vistaLalu.modelo.setValueAt(a.getHistoria(), con, 6);
        vistaLalu.modelo.setValueAt(a.getGeografia(), con, 7);
        vistaLalu.modelo.setValueAt(a.geteFisica(), con, 8);
        vistaLalu.modelo.setValueAt(a.getPromedio(), con, 9);
        con++;
    }

    public void IniciarVistaListaAlumno() {
        vistaLalu.setSize(800, 400);
        vistaLalu.setTitle("Lista");
        vistaLalu.setResizable(false);
        vistaLalu.setLocationRelativeTo(null);
        vistaLalu.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        vistaLalu.setVisible(true);
    }

    public void EventosLA() {
        vistaLalu.botonOk.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                vistaLalu.dispose();
            }
        });
    }

}
