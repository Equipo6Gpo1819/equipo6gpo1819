package Controlador;

import Alumno.Alumno;
import Modelo.ModeloAltaAlumno;
import Vista.VistaAltaAlumno;
import java.awt.event.*;
import java.util.LinkedList;
import javax.swing.*;

public class ControladorAltaAlumno {

    public static LinkedList lista = new LinkedList(); //este es el contenedor de los datos

    private VistaAltaAlumno vistaAlu;
    private ModeloAltaAlumno modeloAlu;

    String clave;
    String apellidoP;
    String apellidoM;
    String nombre;
    String espanol;
    String matematicas;
    String historia;
    String geografia;
    String eFisica;
    String promedio;

    public ControladorAltaAlumno(ModeloAltaAlumno modeloAlu, VistaAltaAlumno vistaAlu) {
        this.vistaAlu = vistaAlu;
        this.modeloAlu = modeloAlu;
        EventosAA();
    }

    public void IniciarVistaAltaAlumno() {
        vistaAlu.setSize(400, 300);
        vistaAlu.setResizable(false);
        vistaAlu.setVisible(true);
        vistaAlu.setLocationRelativeTo(null);
        vistaAlu.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }

    public void EventosAA() {
        //evento que guarda la lista y limpia celdas
        vistaAlu.guardar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {

                clave = vistaAlu.tClave.getText();
                modeloAlu.setClave(clave);

                apellidoP = vistaAlu.tAP.getText();
                modeloAlu.setApellidoP(apellidoP);

                apellidoM = vistaAlu.tAM.getText();
                modeloAlu.setApellidoM(apellidoM);

                nombre = vistaAlu.tNombre.getText();
                modeloAlu.setNombre(nombre);

                espanol = vistaAlu.tEspanol.getText();
                modeloAlu.setEspanol(espanol);

                matematicas = vistaAlu.tMatematicas.getText();
                modeloAlu.setMatematicas(matematicas);

                historia = vistaAlu.tHistoria.getText();
                modeloAlu.setHistoria(historia);

                geografia = vistaAlu.tHistoria.getText();
                modeloAlu.setGeografia(geografia);

                eFisica = vistaAlu.tEfisica.getText();
                modeloAlu.seteFisica(eFisica);

                modeloAlu.sacarPromedio();
                promedio = modeloAlu.getSumaPromedio().toString();
                modeloAlu.setPromedio(promedio);

                Alumno a = new Alumno(modeloAlu.getClave(), modeloAlu.getApellidoP(), modeloAlu.getApellidoM(),
                        modeloAlu.getNombre(), modeloAlu.getEspanol(), modeloAlu.getMatematicas(),
                        modeloAlu.getHistoria(), modeloAlu.getGeografia(), modeloAlu.geteFisica(),
                        modeloAlu.getPromedio());

                lista.add(a);

                vistaAlu.tClave.setText(" ");
                vistaAlu.tAP.setText(" ");
                vistaAlu.tAM.setText(" ");
                vistaAlu.tNombre.setText(" ");
                vistaAlu.tEspanol.setText(" ");
                vistaAlu.tMatematicas.setText(" ");
                vistaAlu.tHistoria.setText(" ");
                vistaAlu.tGeografia.setText(" ");
                vistaAlu.tEfisica.setText(" ");
                JOptionPane.showMessageDialog(null, "Alumno agregado con éxito");
            }
        });

        vistaAlu.ok.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                vistaAlu.dispose();
            }
        });
    }
}