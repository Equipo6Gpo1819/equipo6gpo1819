package Alumno;

import Controlador.ControladorVentanaPrincipal;
import Vista.VistaVentanaPrincipal;

public class EjecutarPrograma {
    
    public static void main(String[] args){
        VistaVentanaPrincipal v = new VistaVentanaPrincipal();
        ControladorVentanaPrincipal vc = new ControladorVentanaPrincipal(v);
        vc.InciarVista();
    }
}