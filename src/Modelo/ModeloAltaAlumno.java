package Modelo;

public class ModeloAltaAlumno{
    
        String clave;
        String apellidoP;
        String apellidoM;
        String nombre;
        String espanol;
        String matematicas;
        String historia;
        String geografia;
        String eFisica;
        Double sumaPromedio;
        String promedio;  

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getApellidoP() {
        return apellidoP;
    }

    public void setApellidoP(String apellidoP) {
        this.apellidoP = apellidoP;
    }

    public String getApellidoM() {
        return apellidoM;
    }

    public void setApellidoM(String apellidoM) {
        this.apellidoM = apellidoM;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEspanol() {
        return espanol;
    }

    public void setEspanol(String espanol) {
        this.espanol = espanol;
    }

    public String getMatematicas() {
        return matematicas;
    }

    public void setMatematicas(String matematicas) {
        this.matematicas = matematicas;
    }

    public String getHistoria() {
        return historia;
    }

    public void setHistoria(String historia) {
        this.historia = historia;
    }

    public String getGeografia() {
        return geografia;
    }

    public void setGeografia(String geografia) {
        this.geografia = geografia;
    }

    public String geteFisica() {
        return eFisica;
    }

    public void seteFisica(String eFisica) {
        this.eFisica = eFisica;
    }  
    
    public Double getSumaPromedio() {
        return sumaPromedio;
    }

    public void setSumaPromedio(Double sumaPromedio) {
        this.sumaPromedio = sumaPromedio;
    }
    
    public String getPromedio() {
        return promedio;
    }

    public void setPromedio(String promedio) {
        this.promedio = promedio;
    }
    
    public void sacarPromedio(){
        sumaPromedio = (Double.parseDouble(espanol)+Double.parseDouble(matematicas)+Double.parseDouble(historia)+
                        Double.parseDouble(geografia)+Double.parseDouble(eFisica))/5;
    }
    
}