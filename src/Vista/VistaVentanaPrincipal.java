package Vista;

import javax.swing.*;

public class VistaVentanaPrincipal extends JFrame {

    public JMenuBar barraMenu;
    public JMenu mArchivo, mOpciones, mVer, mAyuda;
    public JMenuItem oSalir, oAgregar, oVerLista, oAcercaDe;

    public VistaVentanaPrincipal() {
        //Barra
        barraMenu = new JMenuBar();

        //Componentes de la barra
        mArchivo = new JMenu("Archivo");
        oSalir = new JMenuItem("Salir");

        mOpciones = new JMenu("Opciones");
        oAgregar = new JMenuItem("Agregar");

        mVer = new JMenu("Ver");
        oVerLista = new JMenuItem("Ver Lista");

        mAyuda = new JMenu("Ayuda");
        oAcercaDe = new JMenuItem("Acerca de...");

        //Adicionar componentes a la barra
        mArchivo.add(oSalir);
        mOpciones.add(oAgregar);
        mVer.add(oVerLista);
        mAyuda.add(oAcercaDe);
        barraMenu.add(mArchivo);
        barraMenu.add(mOpciones);
        barraMenu.add(mVer);
        barraMenu.add(mAyuda);

        this.setJMenuBar(barraMenu);
    }

}
