package Vista;

import java.awt.*;
import javax.swing.*;

public class VistaAltaAlumno extends JFrame {

    public JTextField tClave, tAP, tAM, tNombre, tEspanol, tMatematicas, tHistoria, tGeografia, tEfisica;
    public JButton guardar, ok;

    public VistaAltaAlumno() {

        //panel que se van a utilizar
        JPanel panelPrincipal = new JPanel(new GridLayout(2, 1));

        JPanel panelGrid1 = new JPanel(new BorderLayout());
        JPanel panelGrid1Norte = new JPanel(new FlowLayout(FlowLayout.CENTER));
        JPanel panelGrid1Centro = new JPanel(new GridLayout(4, 2));

        JPanel panelGrid2 = new JPanel(new BorderLayout());
        JPanel panelGrid2Norte = new JPanel(new FlowLayout(FlowLayout.CENTER));
        JPanel panelGrid2Centro = new JPanel(new GridLayout(3, 4));
        JPanel panelGrid2Sur = new JPanel(new FlowLayout(FlowLayout.CENTER));

        //componentes panel Grid1 norte
        JLabel instruccion = new JLabel("Ingrese Datos");

        //componentes panel Grid1 centro
        JLabel lClave = new JLabel("Clave: ");
        tClave = new JTextField();
        JLabel lAP = new JLabel("Apellido Paterno: ");
        tAP = new JTextField();
        JLabel lAM = new JLabel("Apellido Materno: ");
        tAM = new JTextField();
        JLabel lNombre = new JLabel("Nombre(s): ");
        tNombre = new JTextField();

        //componentes panel Grid2 norte
        JLabel instruccion2 = new JLabel("Ingrese Calificación");

        //componentes panel Grid2 centro
        JLabel lEspañol = new JLabel("Español: ");
        tEspanol = new JTextField();
        JLabel lMatematicas = new JLabel("Matematicas: ");
        tMatematicas = new JTextField();
        JLabel lHistoria = new JLabel("Historia: ");
        tHistoria = new JTextField();
        JLabel lGeografia = new JLabel("Geografia: ");
        tGeografia = new JTextField();
        JLabel lEFisica = new JLabel("Ed. Fisica: ");
        tEfisica = new JTextField();

        //componentes panel Grid2 panel sur
        guardar = new JButton("Guardar");

        ok = new JButton("OK");

        //agregar componentes a paneles
        //grid1
        panelGrid1Norte.add(instruccion);

        panelGrid1Centro.add(lClave);
        panelGrid1Centro.add(tClave);
        panelGrid1Centro.add(lAP);
        panelGrid1Centro.add(tAP);
        panelGrid1Centro.add(lAM);
        panelGrid1Centro.add(tAM);
        panelGrid1Centro.add(lNombre);
        panelGrid1Centro.add(tNombre);

        panelGrid1.add(panelGrid1Norte, BorderLayout.NORTH);
        panelGrid1.add(panelGrid1Centro, BorderLayout.CENTER);
        //grid2
        panelGrid2Norte.add(instruccion2);

        panelGrid2Centro.add(lEspañol);
        panelGrid2Centro.add(tEspanol);
        panelGrid2Centro.add(lMatematicas);
        panelGrid2Centro.add(tMatematicas);
        panelGrid2Centro.add(lHistoria);
        panelGrid2Centro.add(tHistoria);
        panelGrid2Centro.add(lGeografia);
        panelGrid2Centro.add(tGeografia);
        panelGrid2Centro.add(lEFisica);
        panelGrid2Centro.add(tEfisica);

        panelGrid2Sur.add(guardar);
        panelGrid2Sur.add(ok);

        panelGrid2.add(panelGrid2Norte, BorderLayout.NORTH);
        panelGrid2.add(panelGrid2Centro, BorderLayout.CENTER);
        panelGrid2.add(panelGrid2Sur, BorderLayout.SOUTH);

        //agregar paneles al panel principal
        panelPrincipal.add(panelGrid1);
        panelPrincipal.add(panelGrid2);

        this.add(panelPrincipal);
    }   
}