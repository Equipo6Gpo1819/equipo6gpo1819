package Vista;

import java.awt.*;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class VistaListaAlumno extends JFrame {

    public JTable tablaAlumno;
    public JButton botonOk;
    public DefaultTableModel modelo; //para hacer la tabla se usara un modelo
    
    public VistaListaAlumno() {
        //paneles que se van a utilizar
        JPanel panelPrincipal = new JPanel(new BorderLayout());

        JPanel panelNorte = new JPanel(new FlowLayout(FlowLayout.CENTER));
        JPanel panelCentro = new JPanel();
        JPanel panelSur = new JPanel(new FlowLayout(FlowLayout.CENTER));

        //componentes panel
        JLabel titulo = new JLabel("LISTA DE CALIFICACIONES");

        //creamos la tabla usando el modelo
        tablaAlumno = new JTable();

        String[][] datos = {};
        String[] cabecera = {"Clave", "Appellido p.", "Apellido m.", "nombres(s)", "Español",
            "Matematicas", "Historia", "Geografia", "E. Fisica", "Promedio"};

        modelo = new DefaultTableModel(datos, cabecera);
        tablaAlumno.setModel(modelo);

        JScrollPane vistaAlumno = new JScrollPane(tablaAlumno);
        vistaAlumno.setPreferredSize(new Dimension(760, 300));

        botonOk = new JButton("OK");

        //agregar componentes a paneles
        panelNorte.add(titulo);
        panelCentro.add(vistaAlumno);
        panelSur.add(botonOk);

        //integrar paneles
        panelPrincipal.add(panelNorte, BorderLayout.NORTH);
        panelPrincipal.add(panelCentro, BorderLayout.CENTER);
        panelPrincipal.add(panelSur, BorderLayout.SOUTH);
        this.add(panelPrincipal);
    }

}
